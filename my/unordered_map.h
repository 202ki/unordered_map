#include <iostream>
#include <utility>
#include <functional>
#include <type_traits>
#include <cmath>
#include <algorithm>
#include "aditional_structs.h"
#include "dynarray.h"

template <typename Key, typename T, typename Hash = std::hash<Key>, typename KeyEqual = std::equal_to<Key>>
class unordered_map
{
    dynarray<dynarray<std::pair<const Key, T>>> map;
    Hash hashic;
    KeyEqual keyic;
    float factoric = 1.f;
    std::size_t sizic = size();

    using map_type = dynarray<dynarray<std::pair<const Key, T>>>;
    using value_type = std::pair<const Key, T>;
    using size_type = std::size_t;
    using hasher = Hash;
    using key_equal = KeyEqual;
    using iterator = Iterator<map_type, value_type &>;
    using const_iterator = Iterator<map_type const, value_type const &>;
    using node_type = dynarray<value_type>;
public:
    unordered_map() = default;
    unordered_map( size_type bucket_count,
                   const hasher& hash = hasher(),
                   const key_equal& equal = key_equal() )
        : map()
        , hashic(hash)
        , keyic(equal)
    {
        for (size_type i = 0; i < bucket_count; ++i)
            map.emplace_back(node_type());
    }
    template< typename InputIt >
    unordered_map( InputIt first, InputIt last,
                   size_type bucket_count,
                   const hasher& hash = hasher(),
                   const key_equal& equal = key_equal() )
        : map()
        , hashic(hash)
        , keyic(equal)
    {
        for ( auto it = first; it < last; it++ )
        {
            size_type h = hashic(it->first) % bucket_count;
            if (map[0].size() == 0)
                map[h].emplace_back(*it);
        }
    }
    iterator begin()
    {
        iterator it {.ptr = &map};
        return it;
    }
    const_iterator begin() const
    {
        const_iterator it {.ptr = &map};
        return it;
    }
    iterator end()
    {
        if (map.sz()==0)
            return begin();
        iterator it
        {
            .ptr = &map,
            .i1 = map.sz(),
            .i2 = map[map.sz()].sz()
        };
        return it;
    }
    const_iterator end() const
    {
        size_type size = static_cast<size_type>(map.end() - map.begin());
        if (size==0)
            return begin();
        const_iterator it
        {
            .ptr = &map,
            .i1 = size,
            .i2 = map[size].sz()
        };
        return it;
    }
    size_type bucket_count() const 
    {
        size_type sz = static_cast<size_type>(map.end() - map.begin());
        if (sz==0)
            return 1;
        return sz;
    }
    size_type max_bucket_count() const
    {
        return max_size();
    }
    size_type bucket_size( size_type n ) const
    {
        return map[n].end() - map[n].begin();
    }
    size_type bucket( const Key& key ) const
    {
        return hashic(key) % bucket_count();
    }
    bool empty() const
    {
        return (map.end() == map.begin());
    }
    size_type size() const
    {
        size_type sz = static_cast<size_type>(map.end() - map.begin());
        size_type result = 0;
        for (size_type i = 0; i < sz; ++i)
            result += static_cast<size_type>(map[i].end() - map[i].begin());
        return result;
    }
    size_type max_size() const
    {
        return 576460752303423487;
    }
    void clear()
    {
        for (size_type i = 0; i < bucket_count(); ++i)
            map[i].~dynarray();
    }
    std::pair<iterator,bool> insert( const value_type& value )
    {
        return emplace(value);
    }
    std::pair<iterator,bool> insert( value_type&& value )
    {
        return emplace(static_cast<value_type&&>(value));
    }
    template< typename P >
    std::pair<iterator,bool> insert( P&& value )
    {
        if constexpr (std::is_constructible<value_type, P&&>::value == true)
            return insert(static_cast<value_type>(value));
        return  std::make_pair(end(), false);
    }
    template< typename InputIt >
    void insert( InputIt first, InputIt last )
    {
        if (first == last)
            return;
        for (size_type u = 0; u < static_cast<size_type>(last - first); ++u)
            insert(first[u]);
    }
    template< typename... Args >
    std::pair<iterator,bool> emplace( Args&&... args )
    {
        value_type value = {static_cast<Args &&>(args)...};
        if (contains(value.first))
            return std::make_pair(find(value.first), false);
        if (map.sz()==0)
            map.emplace_back(node_type());
        map[0].emplace_back(value);
        rehash(bucket_count());
        iterator result = find(value.first);
        return {result, true};
    }


    iterator erase( iterator pos )
    {
        return erase(const_iterator(pos));
    }
    iterator erase( const_iterator pos )
    {
        delete &(*pos);
        ++pos;
        const Key key = map[pos.i1][pos.i2].first;
        rehash(bucket_count());
        return find(key);
    }
    iterator erase( const_iterator first, const_iterator last )
    {
        for (iterator pos = first; pos < last; ++pos)
            erase(pos);
    }
    size_type erase( const Key& key );


    void swap( unordered_map& other )
    {
        unordered_map b = *this;
        *this = other;
        &other = b;
    }
    template<typename H2, typename P2>
    void merge( unordered_map<Key, T, H2, P2>& source )
    {
        for (auto &i : source)
            insert(i);
    }
    //Lookup
    T& at( const Key& key )
    {
        return &(find(key)->second);
    }
    const T& at( const Key& key ) const
    {
        return &(find(key)->second);
    }
    T& operator[]( const Key&  key )
    {
        if(!contains(key))
        {
            T t;
            emplace(std::make_pair(key, t));
        }
        return &(find(key)->second);
    }
    T& operator[]( Key&& key )
    {
        if(!contains(key))
        {
            T t;
            emplace(std::make_pair(key, t));
        }
        return &(find(key)->second);
    }
    iterator find( const Key& key )
    {
        size_type hash = hashic(key) % bucket_count();
        if (map.sz()==0)
            return end();
        if (map[hash].sz()!=0)
            for (size_type i = 0; i < map[hash].sz(); ++i)
                if (keyic(map[hash][i].first, key))
                    return {.ptr = &map, .i1 = hash, .i2 = i};
        return end();
    }
    const_iterator find( const Key& key ) const
    {
        size_type hash = hashic(key) % bucket_count();
        if (map.sz()==0)
            return end();
        if (map[hash].sz()!=0)
            for (size_type i = 0; i < map[hash].sz(); ++i)
                if (keyic(map[hash][i].first, key))
                {
                    const_iterator it{.ptr = &map, .i1 = hash, .i2 = i};
                    return it;
                }
        return end();
    }
    bool contains( const Key& key ) const
    {
        return find(key)!=end();
    }
    //Hash policy
    float load_factor() const
    {
        return static_cast<float>(sizic) / static_cast<float>(bucket_count());
    }
    float max_load_factor() const
    {
        return factoric;
    }
    void  max_load_factor( float ml )
    {
        factoric = ml;
    }
    void rehash( size_type count )
    {
        count = std::max(sizic / static_cast<size_type>(max_load_factor()), count);
        unordered_map<Key, T> copy = *this;
        new (this) unordered_map<Key, T>(count);
        for(value_type element : copy)
        {
            size_type index = hashic(element.first) % count;
            (map[index]).emplace_back(element);
        }
    }
    void reserve( size_type count )
    {
        rehash(std::ceil(count / max_load_factor()));
    }
    //Observers
    hasher hash_function() const
    {
        return hashic;
    }
    key_equal key_eq() const
    {
        return keyic;
    }
}; //class unordered_map

//Non-member functions
template <typename Key, typename T, typename Hash, typename KeyEqual>
void swap( unordered_map<Key, T, Hash, KeyEqual>& lhs,
          unordered_map<Key, T, Hash, KeyEqual>& rhs )
{
   unordered_map<Key, T, Hash, KeyEqual> b = lhs;
   &lhs = rhs;
   &rhs = b;
}
