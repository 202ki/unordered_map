#include<iostream>

template<typename T>
class dynarray
{
    T *ptr;
    std::size_t size, capacity;
    void reallocate(std::size_t n)
    {
        if (n > capacity)
        {
            T *NEW = static_cast<T *>(std::malloc(sizeof(T) * n));
            for (std::size_t i = 0; i < size; ++i)
            {
                new (NEW+i) T(static_cast<T &&>(*(ptr+i)));
                (ptr+i)->~T();
            }
            std::free(ptr);
            ptr = NEW;
            capacity = n;
        }
    };
public:
    ~dynarray()
    {
        for(std::size_t i = 0; i < size; ++i)
            (ptr + i)->~T();
        std::free(ptr);
    }
    dynarray()
        : ptr(nullptr), size(0), capacity(0)
    {}
    T &operator[](std::size_t i)
    {
        return ptr[i];
    }
    T &operator[](std::size_t i) const
    {
        return ptr[i];
    }
    dynarray(dynarray<T> const &d)
        : dynarray<T>()
    {
        reallocate(d.capacity);
        size = d.size;
        for (std::size_t i = 0; i < size; ++i)
            new (ptr + i) T(d[i]);
    }
    dynarray(dynarray<T> &&d)
        : ptr(d.ptr)
        , size(d.size)
        , capacity(d.capacity)
    {
        new (&d) dynarray<T>();
    }
    std::size_t sz() const
    {
        return size;
    }

    std::size_t cp()const
    {
        return capacity;
    }
    T       *begin()       {return ptr;}
    T       *  end()       {return ptr + size;}
    T const *begin() const {return ptr;}
    T const *  end() const {return ptr + size;}
    template<typename... Args>
    void emplace_back(Args &&... args)
    {
        if(size == capacity)
            reallocate(capacity == 0 ? 1 : 2 * capacity);
        new (ptr + size++) T(static_cast<Args &&>(args)...);
    }
    void push_back(T const &value)
    {
        if(size == capacity)
            reallocate(capacity == 0 ? 1 : 2 * capacity);
        new (ptr + size++) T(value);
    }
    void push_back(T &&value)
    {
        if(size == capacity)
            reallocate(capacity == 0 ? 1 : 2 * capacity);
        new (ptr + size++) T(static_cast<T &&>(value));
    }
};

