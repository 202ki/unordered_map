#include<iostream>

template<typename T, typename V>
struct Iterator
{
    T *ptr = nullptr;
    std::size_t i1 = 0;
    std::size_t i2 = 0;

    Iterator &operator++()
    {
        if (ptr[i1].cp() - i2 <0 )
            i2+=1;
        else
        {
            i2=0;
            i1+=1;
        }
        return *this;
    };
    bool operator!=(Iterator<T, V> const iter) const
    {
        return !(i1==iter.i1 && i2==iter.i2 && ptr == iter.ptr);
    };
    V operator*() const
    {
        return (*ptr)[i1][i2];
    };

    template<typename U, typename W>
    operator Iterator<U, W>() const noexcept
    {
        return
        {
            .ptr = ptr,
            .i1 = i1,
            .i2 = i2,
        };
    }
};
