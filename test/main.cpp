#include <my/unordered_map.h> 
#include <gtest/gtest.h>

TEST(unordered_map, empty ) // слева то что я проверяю, а справа функция
{
    unordered_map<int, int> m; // тип значения тип ключа
    EXPECT_EQ(true, m.empty()); // expect_eq ожидается равныйц, над ним пишу весь код, внутри я справа пишу то что получила после проверок, а слева то что хочу получить
}
TEST(unordered_map, empty_not_empty)
{
    unordered_map<char, char> m (7);
    EXPECT_EQ(false, m.empty());
}

TEST(unordered_map, max_load_fuck ) // слева то что я проверяю, а справа функция
{
    unordered_map<int, int> m; // тип значения тип ключа
    EXPECT_EQ(1.f, m.max_load_factor()); // expect_eq ожидается равныйц, над ним пишу весь код, внутри я справа пишу то что получила после проверок, а слева то что хочу получить
}
TEST(unordered_map, max_load_fuck_1) // слева то что я проверяю, а справа функция
{
    unordered_map<int, int> m; // тип значения тип ключа
    m.max_load_factor(5);
    EXPECT_EQ(5.f, m.max_load_factor()); // expect_eq ожидается равныйц, над ним пишу весь код, внутри я справа пишу то что получила после проверок, а слева то что хочу получить
}
TEST(unordered_map, itbegin)
{
    unordered_map<int, int> m;
    auto begin = m.begin();
    EXPECT_EQ(0u, begin.i1);
}
TEST(unordered_map, itbeginc)
{
    const unordered_map<int, int> m;
    auto begin = m.begin();
    EXPECT_EQ(0u, begin.i2);
}
TEST(unordered_map, end)
{
    unordered_map<int, int> m;
    auto end = m.end();
    EXPECT_EQ(0u, end.i1);
}

TEST(unordered_map, itendc)
{
    const unordered_map<int, int> m;
    auto end = m.end();
    EXPECT_EQ(0u, end.i1);
}
TEST(unordered_map, bucket_count)
{
    const unordered_map<int,int> m;
    EXPECT_EQ(1u, m.bucket_count());
}
TEST(unordered_map, bucket_count99)
{
    const unordered_map<char,char> m (7);
    EXPECT_EQ(7u, m.bucket_count());
}
TEST(unordered_map, insert)
{
    unordered_map<int, int > m;
    std::pair<int, int> pair = {1, 2};
    bool bib = m.insert(pair).second;
    EXPECT_EQ(true, bib);
}
TEST(unordered_map, insert1)
{
    unordered_map<int, int > m;
    std::pair<int, int> pair = {1, 2};
    m.insert(pair);
    bool bibble = m.insert(pair).second;
    EXPECT_EQ(false, bibble);
}
TEST(unordered_map, erase)
{
    unordered_map<int, int > m;
    std::pair<int, int> pair = {1, 2};
    m.insert(pair);
    auto it = m.find(1);
    m.erase(it);
    EXPECT_EQ(true, m.empty());
}
TEST(unordered_map, const_erase)
{
    unordered_map<int, int > m;
    std::pair<int, int> pair = {1, 2};
    m.insert(pair);
    const unordered_map<int, int > n = m;
    auto it = n.find(1);
    m.erase(it);
    EXPECT_EQ(true, m.empty());
}
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
